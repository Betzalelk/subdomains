from flask import Flask
from flask import request
from flask import Response

import subdomains

app = Flask(__name__)


@app.route('/')
def Main():
    return 'Welcome to the API!'

@app.route('/subsomains', methods=['GET'])
def GetSubdomains():
    if request.method == "GET":
        domain = request.args.get('domain')
        result = subdomains.getAll(domain)
        resp = Response(response=result,
                    status=200,
                    mimetype="application/json")
        return resp


if __name__ == "__main__":
    app.run(debug = True, host = "0.0.0.0")