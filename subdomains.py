import sublist3r
import json
import requests
import os
import socket


def getAll(domain):
    print('working on domain:', domain)
    try:
        subdomains = sublist3r.main(domain, 40, f'{domain}.txt',  ports=None, silent=False, verbose=False, enable_bruteforce=False, engines=None)
        print(subdomains)
    except Exception as e:
        print(e)
    return analyseSubdomains(f'{domain}.txt') 

def analyseSubdomains(fileName):
    res = {}
    try:
        with open(fileName) as f:
            lines = [line.rstrip() for line in f]
        deleteFile(fileName) 
        for domain in lines:
            res[domain] = {"headers": [], "total_subdomains": len(lines)}
            print(f'work on subdomain: {domain}')
            try:
                httpr = requests.get(f'http://{domain}', timeout=30)

                res[domain]["http_res_status"] =  httpr.status_code
                if httpr.headers['content-type']:
                    res[domain]["content_type"] = httpr.headers['content-type']
                for header, value in httpr.headers.items():
                    res[domain]["headers"].append({header: value})
            except Exception as e:
                print(e)
          
            if "http_res_status" not in res[domain].keys():
                try:
                    httpsr = requests.get(f'https://{domain}', timeout=30)
                    res[domain]["https_res_status"] =  httpsr.status_code
                    if httpsr.headers['content-type']:
                        res[domain]["content_type"] = httpsr.headers['content-type']
                    for header, value in httpsr.headers.items():
                        res[domain]["headers"].append({header: value})
                except Exception as e:
                    print(e)
            try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                result = sock.connect_ex((domain, 80))
                if result == 0:
                    res[domain]["port_80"] = 'open'
                else:
                    res[domain]["port_80"] = 'close'
            except Exception as e:
                print(e)
            finally:
                sock.close()

            try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                result = sock.connect_ex((domain, 443))
                if result == 0:
                    res[domain]["port_443"] = 'open'
                else:
                    res[domain]["port_443"] = 'close'
            except Exception as e:
                print(e)
            finally:
                sock.close()
                     
        return json.dumps(res)
    except Exception as e:
        print(e)
        return json.dumps({"error": e.args})

def getError(e):
    return f'{e}'

def deleteFile(fileName):
    if os.path.exists(fileName):
        os.remove(fileName)
        print(f'delete file: {fileName}')
    else:
        print(f'The file {fileName} does not exist')